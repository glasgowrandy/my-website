import axios from 'axios'



// The connector used for hitting the endpoints.
export default () => {
    return axios.create({
        BaseURL: 'localhost:8081/'
    })
}